import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'exceptions/network_exceptions.dart';

class RestClient {
  //GET
  final JsonDecoder _decoder = JsonDecoder();

  Future<dynamic> get(String url) {
    return http.get(url).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw NetworkException(
          message: "Error fetching data from server",
          statusCode: statusCode,
        );
      }
      return _decoder.convert(res);
    });
  }

  //POST

}
