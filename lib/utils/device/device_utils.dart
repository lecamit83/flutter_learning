import 'package:flutter/material.dart';
class DeviceUtilts {
  static hideKeyboard(BuildContext context) {
    FocusScope.of(context).unfocus();
  }
}